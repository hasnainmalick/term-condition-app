// @ts-nocheck
// @ts-ignore
import React, { useState, useEffect } from "react";
import {
  reactExtension,
  Checkbox,
  Link,
  useApi,
  useBuyerJourneyIntercept,
  useExtensionCapability,
  useApplyNoteChange,
  View,
} from "@shopify/ui-extensions-react/checkout";

export default reactExtension("purchase.checkout.block.render", () => (
  <CheckBoxLinks />
));

const APP_URI = "https://s-tos.sweepefy.com";
// const APP_URI = "https://then-insights-elegant-administrator.trycloudflare.com";

export const CheckBoxLinks = () => {
  // Merchants can toggle the `block_progress` capability behavior within the checkout editor
  const [validationError, setValidationError] = useState("");
  const [checked, setChecked] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const [altMsg, setAltMsg] = useState("");
  const [enable, setEnable] = useState(true);
  const [editorLink, setEditorLink] = useState("");
  const [attribute, setAttribute] = useState("");
  const [consent, setConsent] = useState(true);
  const [optionalChecked, setOptionalChecked] = useState(false);
  const [editorHyperText, setEditorHyperText] = useState("");
  const { sessionToken } = useApi();
  const note = useApplyNoteChange();
  async function getToken() {
    const token = await sessionToken.get();
    return token;
  }
  useEffect(() => {
    getToken().then((token) => {
      // console.log("This is token :: ", token)
      fetch(`${APP_URI}/app/checkboxStatus`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          // console.log("data :: ",data)
          if (data.AppearnceDetail.length > 0) {
           
            if (data.AppearnceDetail[0]?.checkbox_optional) {
              setOptionalChecked(
                JSON.parse(data.AppearnceDetail[0].checkbox_optional)
              );
            }
            if (data.AppearnceDetail[0]?.checkbox_by_default) {
              setChecked(
                JSON.parse(data.AppearnceDetail[0].checkbox_by_default)
              );
            }
            if (data.AppearnceDetail[0]?.consent) {
              setConsent(JSON.parse(data.AppearnceDetail[0].consent));
            }
          }
          if (data?.active) {
            setEnable(JSON.parse(data?.active));
          }
          if (data.ErrorResponse[0]?.editorHyperLinkText) {
            setEditorHyperText(data.ErrorResponse[0]?.editorHyperLinkText);
          }
          setAttribute(data.AppearnceDetail[0]?.attribute_name ?? "");
          setEditorLink(data.ErrorResponse[0]?.link ?? "");
          setErrorMsg(data.ErrorResponse[0]?.errormsg ?? "");
          setAltMsg(data.ErrorResponse[0]?.altmsg ?? "");
          // setEnable(data.active);
        })
        .catch((error) => console.error("Error:", error));
    });
  }, [enable]);

  const canBlockProgress = useExtensionCapability("block_progress");
  useBuyerJourneyIntercept(({ canBlockProgress }) => {
    // Validate that the age of the buyer is known, and that they're old enough to complete the purchase
    if (optionalChecked) {
      return {
        behavior: "allow",
        perform: () => {
          // Ensure any errors are hidden
          clearValidationErrors();
        },
      };
    } else if (canBlockProgress && !checked && enable) {
      return {
        behavior: "block",
        reason: "Checked is required",
        perform: (result) => {
          // If progress can be blocked, then set a validation error on the custom field
          if (result.behavior === "block") {
            setValidationError(altMsg);
          }
        },
      };
    }

    return {
      behavior: "allow",
      perform: () => {
        // Ensure any errors are hidden
        clearValidationErrors();
      },
    };
  });

  // Initialize a flag for post request

  const handleChange = async () => {
    setChecked((value) => !value);
    if (consent) {
      const date = new Date();
      const formattedDateTime = attribute + " " + date.toLocaleString();
      await note({
        type: "updateNote",
        note: formattedDateTime,
      });
    }
  };

  // @ts-ignore
  // function isCheckBoolen() {
  //   return checked == checktTarget;
  // }
  function clearValidationErrors() {
    setValidationError("");
  }
 
  return (
    <View>
      {enable && (
        <>
          <Checkbox
            id="checkbox1"
            name="checkboxchoices"
            value={checked}
            onChange={handleChange}
            error={validationError}
          >
            {errorMsg && <>{errorMsg}</>}
            {editorHyperText && (
              <>
                {" "}
                <Link to={editorLink}>{editorHyperText}</Link>
              </>
            )}
          </Checkbox>
        </>
      )}
    </View>
  );
};
