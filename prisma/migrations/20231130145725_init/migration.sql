/*
  Warnings:

  - You are about to drop the `checkbox` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[shop]` on the table `errormsg` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE `errormsg` ADD COLUMN `altmsg` VARCHAR(255) NULL,
    ADD COLUMN `link` VARCHAR(250) NULL,
    MODIFY `errormsg` VARCHAR(255) NULL;

-- DropTable
DROP TABLE `checkbox`;

-- CreateTable
CREATE TABLE `checkoutcheckbox` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(191) NOT NULL,
    `date` VARCHAR(191) NOT NULL,
    `time` VARCHAR(191) NOT NULL,
    `shop` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `active_checkbox` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `active` VARCHAR(50) NOT NULL,
    `shop` VARCHAR(50) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `appearnce_setting` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `checkbox_by_default` VARCHAR(50) NULL,
    `checkbox_optional` VARCHAR(50) NULL,
    `consent` VARCHAR(50) NULL,
    `attribute_name` VARCHAR(250) NULL,
    `shop` VARCHAR(50) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `shop` ON `errormsg`(`shop`);
