-- CreateTable
CREATE TABLE `Errormsg` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `errormsg` VARCHAR(191) NULL,
    `shop` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
