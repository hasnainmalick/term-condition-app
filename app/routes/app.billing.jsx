// @ts-nocheck
import React, { useCallback, useEffect, useState } from "react";
import { Button, Card, Frame, Page, Toast } from "@shopify/polaris";
import { authenticate, MONTHLY_PLAN } from "../shopify.server";
import { Form, useActionData } from "@remix-run/react";
import { json } from "@remix-run/node";
// import { Billing } from '../components/Dashboard/Billing.jsx'
export async function action({ request }) {
    const { billing } = await authenticate.admin(request);
    const billingCheck = await billing.require({
        plans: [MONTHLY_PLAN],
        isTest: true, 
        onFailure: async () => billing.request({
            plan: MONTHLY_PLAN,
            // isTest:true,
            // returnUrl:'/app',
        }),
    });
    // const subscription = billingCheck.appSubscriptions[0];
    // const cancelledSubscription = await billing.cancel({
    //   subscriptionId: subscription.id,
    //   isTest: true,
    //   prorate: true,
    //  });
    // console.log("This is billing : ", cancelledSubscription);
    return json(billingCheck);
    // const subscription = billingCheck.appSubscriptions[0];
    // console.log(`Shop is on ${subscription.name} (id ${subscription.id})`);
}
const BillingComponent = () => {
    const [active, setActive] = useState(false);

  const toggleActive = useCallback(() => setActive((active) => !active), []);

  const toastMarkup = active ? (
    <Toast content="You already selected a plan" onDismiss={toggleActive} />
  ) : null;
    const data = useActionData();
    useEffect(() => {
        if (data?.hasActivePayment === true) {
          toggleActive();
        }
      }, [data]);
    // console.log("This is data",data)
    return (
        <Page title="Manage Subscription" narrowWidth backAction={{content: '', url: '/app'}}>
            <Frame>
            <Card>
                <Form method="post">
                    <div
                        style={{
                            textAlign: "center",
                            display: "flex",
                            flexDirection: "column",
                        }}
                    >
                        <h1 style={{ fontSize: "33px", marginBottom: "19px" }}>
                            Pricing Plan
                        </h1>
                        <span style={{ fontSize: "13px" }}>No additional charges</span>
                        <b>$4.99/month</b>
                        <span style={{ fontSize: "13px" }}>7 days free trial</span>
                        <div>
                            <Button primary submit>
                                Choose Plan
                            </Button>
                            {toastMarkup}
                        </div>
                    </div>
                </Form>
            </Card>
            </Frame>
        </Page>
    );
};

export default BillingComponent;
