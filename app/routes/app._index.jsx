// @ts-nocheck
import { useEffect ,useState,useCallback} from "react";
import { json } from "@remix-run/node";
import { Form, useActionData, useLoaderData, useNavigation, useSubmit } from "@remix-run/react";
// import Dashboard from "~/components/Dashboard.jsx";
import db from "../db.server.js"
import { getAppearanceDetails, getCheckboxDetail, getErrorandAltDetails , InsertCustomErrorMsg} from "~/models/termsCondition.server.js";
import { Page,} from "@shopify/polaris";

import { authenticate } from "../shopify.server";
import TopbarTabs from "~/components/Index.jsx";
// import TopbarTabs from "~/components/Tabs.jsx"
export const loader = async ({ request }) => {
  const {session} = await authenticate.admin(request);
  const response = await getErrorandAltDetails(session.shop);
  const activeCheck= await getCheckboxDetail(session.shop)
  const appearnceSetting = await getAppearanceDetails(session.shop)
  
  // const activeCheckbox = activeCheck[0]?.active;
  // console.log("activeCheckbox ,", activeCheckbox)
  // if (!activeCheckbox && response) {
  //   return json({ response, activeCheck: false,appearnceSetting });
  // }
  // else if(!response && activeCheckbox){
  //   return json({ response:[{id:1,errormsg:"",altmsg:""}], activeCheck:activeCheck[0]?.active,appearnceSetting});

  // }else if(!response && !activeCheckbox){
  //   return json({response:[{id:1,errormsg:"",altmsg:""}], activeCheck:false,appearnceSetting });
  // }else{
    return json({ response, activeCheck:activeCheck[0]?.active,appearnceSetting});
  
};

export async function action({ request }) {
  const { session } = await authenticate.admin(request);
  const { shop } = session;
  const formData = await request.formData();
  const noteData = Object.fromEntries(formData);
  const { ErrorMsg,altMsg,editorLinks ,editorHyperText} = noteData;

  if(ErrorMsg != "" && altMsg != ""){
    await InsertCustomErrorMsg(ErrorMsg,altMsg ,editorLinks,editorHyperText,shop)
    // @ts-ignore
  }
  return json({msg:"ok"});
 
}

export default function Index() {
  const {response,activeCheck,appearnceSetting} = useLoaderData();
  // eslint-disable-next-line no-unused-expressions, array-callback-return
  // console.log("Index",activeCheckbox)
  // eslint-disable-next-line no-const-assign
  const ErrorandAltMsg = response.map(item => ({
    id: item.id,
    errormsg: item.errormsg,
    altmsg: item.altmsg,
    editorLinks:item.link,
    editorHyperLinkText:item.editorHyperLinkText
  }));
  return (
    <Page fullWidth>
      <TopbarTabs ErrorandAltMsg={ErrorandAltMsg} activeCheckbox={activeCheck} appearnceSetting={appearnceSetting}/>
        
    </Page>
  );
}
