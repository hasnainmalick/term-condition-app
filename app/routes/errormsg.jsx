import { json } from "@remix-run/node";
import db from "../db.server"

export async function loader({ request }) {
  //TODO: I have to add shop for validation
    // @ts-ignore
    const response = await db.errormsg.findMany({
      orderBy: {
        id: "desc"
      },
      take: 1,
    });
    return json({ response:response },
    {
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
    });
  }