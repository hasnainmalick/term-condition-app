import { json } from "@remix-run/node";
import { authenticate,MONTHLY_PLAN } from "../shopify.server";

export async function loader({ request }) {
  await authenticate.admin(request);
  const { billing } = await authenticate.admin(request);
  const billingCheck = await billing.require({
    plans: [MONTHLY_PLAN],
    isTest: true,
    onFailure: async () => billing.request({ plan: MONTHLY_PLAN }),
  });

  return json(billingCheck);
}
