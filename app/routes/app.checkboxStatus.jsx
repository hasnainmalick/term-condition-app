import { json } from "@remix-run/node";
import db from "../db.server.js";
import { authenticate } from "~/shopify.server.js";
import {
  InsertActiveCheckbox,
  getAppearanceDetails,
  getCheckboxDetail,
  getErrorandAltDetails,
} from "~/models/termsCondition.server.js";
export async function action({ request }) {
  const { session } = await authenticate.admin(request);
  const { shop } = session;
  const formData = await request.formData();
  const { active } = Object.fromEntries(formData);
  const resp = await InsertActiveCheckbox(active, shop);
  
  return json({ msg:resp});
}
export const loader = async ({ request }) => {
  const { sessionToken ,cors} = await authenticate.public.checkout(request);
  const response = await getCheckboxDetail(sessionToken?.dest);
  const ErrorResponse = await getErrorandAltDetails(sessionToken?.dest);
  const AppearnceDetail = await getAppearanceDetails(sessionToken?.dest);
  const active = response[0]?.active;
  return cors(json({active,ErrorResponse,AppearnceDetail}));
};