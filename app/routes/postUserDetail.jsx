import { json } from "@remix-run/node";
import db from "../db.server";
export async function action({ request }) {
  const { email,shop, date } = await request.json();
  const filterTime = new Date(date).toLocaleTimeString();
  const filterDate = new Date(date).toLocaleDateString();
  const temp = {
    email:email,
    date: filterDate,
    time: filterTime,
    shop: shop,
  };
  const response = await db.checkoutcheckbox.create({
    data: temp,
  });
  return json(
    { msg: "done" },
    {
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
    }
  );
}
