import { json } from "@remix-run/node";
import { InsertAppearanceSettingdata } from "~/models/termsCondition.server";
import { authenticate } from "~/shopify.server";
export async function action({ request }) {
    const { session } = await authenticate.admin(request);
    const { shop } = session;
    const formData = await request.formData();
    const data = Object.fromEntries(formData);
   // const attribute = data?.attribute !== undefined ? data.attribute : "";
    // const checkbox1 = data?.checkbox1 !== undefined ? data.checkbox1 : "";
    // const checkbox2 = data?.checkbox2 !== undefined ? data.checkbox2 : "";
    // const OrderConsent = data?.active !== undefined ? data.active : "";
    await InsertAppearanceSettingdata(data,shop)
    // console.log("checkboxState ::sd",data, attribute,  checkbox1,  checkbox2, OrderConsent)
    // const resp = await InsertActiveCheckbox(active, shop);
    
    return json({ msg:"ok"});
  }