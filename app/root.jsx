import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  
} from "@remix-run/react";
import { cssBundleHref } from "@remix-run/css-bundle";
import AppContextProvider from "./context/AppContextProvider";
export default function App() {
  return (
    <html>
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body>
        <AppContextProvider>
        <Outlet />
        <ScrollRestoration />
        <LiveReload />
        <Scripts />
        </AppContextProvider>
      </body>
    </html>
  );
}
export const links= () => [
  { rel: "stylesheet", href: cssBundleHref },
];