// @ts-nocheck
import { json } from "@remix-run/node";
import db from "../db.server";
export async function getErrorandAltDetails(shop) {
  // @ts-ignore
  const response = await db.errormsg.findMany({
    where: {
      shop: shop,
    },
  });
  return Promise.all(response);
}

export async function InsertCustomErrorMsg(
  errorMsg,
  altMsg,
  editorLinks,
  editorHyperLinkText,
  shop
) {
  const data = {
    errormsg: errorMsg,
    altmsg: altMsg,
    link: editorLinks,
    editorHyperLinkText: editorHyperLinkText,
    shop: shop,
  };

  const existingData = await db.errormsg.findMany({
    where: { shop: shop },
  });
  if (existingData.length > 0) {
    // Data exists, update it
    await db.errormsg.update({
      where: { id: existingData[0]?.id },
      data: data,
    });
  } else {
    // Data doesn't exist, insert new row
    await db.errormsg.create({
      data: data,
    });
  }

  return json({
    msg: "ok",
  });
}

export async function InsertActiveCheckbox(active, shop) {
  const data = {
    active: active,
    shop: shop,
  };

  const existingData = await db.active_checkbox.findMany({
    where: { shop: shop },
  });
  if (existingData.length > 0) {
    // Data exists, update it
    const response = await db.active_checkbox.update({
      where: { id: existingData[0]?.id },
      data: data,
    });
    return json({
      msg: response,
    });
  } else {
    // Data doesn't exist, insert new row
    const response = await db.active_checkbox.create({
      data: data,
    });
    return json({
      msg: response,
    });
  }
}
export async function getCheckboxDetail(shop) {
  // @ts-ignore
  const response = await db.active_checkbox.findMany({
    where: {
      shop: shop,
    },
  });
  return Promise.all(response);
}
export async function InsertAppearanceSettingdata(appearnceData, shop) {
  // console.log("appearnceData :: ", appearnceData);

  const data = {
    checkbox_by_default:
      appearnceData?.checkbox1 !== undefined ? appearnceData.checkbox1 : "",
    checkbox_optional:
      appearnceData?.checkbox2 !== undefined ? appearnceData.checkbox2 : "",
    consent: appearnceData?.active !== undefined ? appearnceData.active : "",
    attribute_name:
      appearnceData?.attribute !== undefined ? appearnceData.attribute : "",
    shop: shop,
  };

  const existingData = await db.appearnce_setting.findMany({
    where: { shop: shop },
  });

  if (existingData.length > 0) {
    // Data exists, update it
    await db.appearnce_setting.update({
      where: { id: existingData[0]?.id },
      data: {
        checkbox_by_default: data.checkbox_by_default || undefined,
        checkbox_optional: data.checkbox_optional || undefined,
        consent: data.consent || undefined,
        attribute_name: data.attribute_name || undefined,
        shop: data.shop,
      },
    });
  } else {
    // Data doesn't exist, insert new row
    const response = await db.appearnce_setting.create({
      data: data,
    });
    return json({
      msg: response,
    });
  }
}

export async function getAppearanceDetails(shop) {
  // @ts-ignore
  const response = await db.appearnce_setting.findMany({
    where: {
      shop: shop,
    },
  });
  return Promise.all(response);
}
