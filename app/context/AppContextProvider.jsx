// @ts-nocheck
import React, { useState,useReducer ,useCallback} from 'react'
import { AppContext } from './AppContext'
const ActionTypes = {
  SET_INPUT: 'SET_INPUT',
  SET_ALT_VALUE: 'SET_ALT_VALUE',
  SET_SELECTED: 'SET_SELECTED',
  TOGGLE_ACTIVE: 'TOGGLE_ACTIVE',
  LINK_INPUT:'LINK_INPUT',
  EDITOR_HYPER_TEXT:'EDITOR_HYPER_TEXT',
  HANDLE_ACTIVE:'HANDLE_ACTIVE'
};

// Define initial state
const initialState = {
  input: '',
  altValue: '',
  selected: 0,
  active: true,
  editorLink:'',
  editorHyperLinkText:'',
};

// Define reducer function
const reducer = (state, action) => {
  switch (action.type) {
    case ActionTypes.SET_INPUT:
      return { ...state, input: action.payload };
    case ActionTypes.SET_ALT_VALUE:
      return { ...state, altValue: action.payload };
    case ActionTypes.SET_SELECTED:
      return { ...state, selected: action.payload };
    case ActionTypes.TOGGLE_ACTIVE:
      return { ...state, active: !state.active }; 
    case ActionTypes.HANDLE_ACTIVE:
      return { ...state, active: action.payload }; 
    case ActionTypes.LINK_INPUT:
      return { ...state, editorLink: action.payload };
      case ActionTypes.EDITOR_HYPER_TEXT:
      return { ...state, editorHyperLinkText: action.payload };
    default:
      return state;
  }
};

const AppContextProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [OrderConsent,setOrderConsent] = useState(true)
  const [attributeName,setAttributeName]=useState("");
  const [checked, setChecked] = useState(false);
  const [optionalChecked, setOptionalChecked] = useState(false);
  const handleCheckboxChange = (checkbox1, checkbox2) => {
    setChecked(checkbox1);
    setOptionalChecked(checkbox2);
  
    const formData = new FormData();
    formData.append("checkbox1", checkbox1);
    formData.append("checkbox2", checkbox2);
  
    fetch("/app/appearnceSetting", {
      method: "POST",
      body: formData,
    });
  };
  const handleOptionalCheck=(checkbox2)=>{
    setOptionalChecked(checkbox2);
  }
  const handleDefaultCheck=(checkbox1)=>{
    setChecked(checkbox1);
  }
  const handleAttribueName = useCallback((newValue) => setAttributeName(newValue), []);
  const handleAppearnceChecked=()=>{
    setOrderConsent(value => !value)
  }
  const handleAppearnceCheckedDB=(check)=>{
    setOrderConsent(check)
  }
  const handleChange = useCallback((newValue) => dispatch({ type: ActionTypes.SET_INPUT, payload: newValue }), []);
  const handleActiveCheckbox = useCallback(() => dispatch({ type: ActionTypes.TOGGLE_ACTIVE }), []);
  const handleActiveCheckboxDB = useCallback((newValue) => dispatch({ type: ActionTypes.HANDLE_ACTIVE,  payload: newValue}), []);
  const handleAltChange = useCallback((newValue) => dispatch({ type: ActionTypes.SET_ALT_VALUE, payload: newValue }), []);
  const handleTabChange = useCallback((selectedTabIndex) => dispatch({ type: ActionTypes.SET_SELECTED, payload: selectedTabIndex }), []);
  const handleEditorLink = useCallback((newValue) => dispatch({ type: ActionTypes.LINK_INPUT,  payload: newValue }), []);
  const handleEditorHyperText = useCallback((newValue) => dispatch({ type: ActionTypes.EDITOR_HYPER_TEXT,  payload: newValue }), []);
  const {input, altValue, selected,active,editorLink,editorHyperLinkText} = state
  return (
    <AppContext.Provider value={{input,handleChange,altValue,handleAltChange,selected,handleTabChange ,active , handleActiveCheckbox,editorLink,handleEditorLink,handleActiveCheckboxDB,OrderConsent,handleAppearnceChecked,handleAppearnceCheckedDB,handleAttribueName,attributeName,checked,
      optionalChecked,handleOptionalCheck, handleDefaultCheck, handleCheckboxChange, editorHyperLinkText, handleEditorHyperText}}>
      {children}
    </AppContext.Provider>
  )
}

export default AppContextProvider
