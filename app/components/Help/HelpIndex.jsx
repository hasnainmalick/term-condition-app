import { Divider, Page } from "@shopify/polaris";
import React from "react";
import HelpHeader from "./HelpHeader";
import FAQ from "./FAQ";
import ContactUs from "./ContactUs"
const HelpIndex = () => {
  return (
    <Page>
      <HelpHeader />
      <p style={{ marginBottom: "10px" }}></p>
      <Divider />
      <FAQ/>
      <p style={{ marginBottom: "20px" }}></p>
      <Divider />
      <ContactUs/>
    </Page>
  );
};

export default HelpIndex;
