// @ts-nocheck
import React,{useContext} from "react";
import { Button,  Text } from "@shopify/polaris";
import { MobileBackArrowMajor } from "@shopify/polaris-icons";
import styles from "../index.module.css";
import { AppContext } from "~/context/AppContext";
const HelpHeader = () => {
  const { handleTabChange } = useContext(AppContext)
   return (
    <div className={styles.ApperacnceAccount}>
      <Button onClick={()=> handleTabChange(0)}icon={MobileBackArrowMajor}></Button>{" "}
      <p className={styles.spacing}></p>
      <Text variant="headingLg" as="h5">
        Help
      </Text>
    </div>
  );
};

export default HelpHeader;
