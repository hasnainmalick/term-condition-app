// @ts-nocheck
import React, { useState, useCallback } from "react";
import { Text, Card, Link, Checkbox, List, Button, ButtonGroup } from "@shopify/polaris";
import styles from "../index.module.css";
const ContactUs = () => {
    const [checked, setChecked] = useState(false);
    const [pressedBtn,setPressedBtn] = useState(false);
    const handlePressed=()=>{
        setPressedBtn(value => !value)
    }
    const handleChange = useCallback((newChecked) => setChecked(newChecked), []);
    return (
        <div>
            <div className={styles.ParentCheckboxAppearnce}>
            <div className={styles.SecondChildCheckboxStatus}>
                    <Card>
                        <Text as="p" variant="bodyMd" fontWeight="regular">
                            Still need help? Want to suggest a feature? No worries!
                            <br />
                            We are available via chat almost every day of the week!
                        </Text>
                        <div className={styles.HelpButtonGroup}>
                            <ButtonGroup>
                                <Button pressed={pressedBtn} onClick={handlePressed} target="_blank" url="https://webla.io/contact/">Contact Us</Button>
                            </ButtonGroup>
                        </div>
                    </Card>
                </div>
                <div className={styles.FirstChildCheckboxStatus}>
                    <Text as="h2" variant="bodyMd" fontWeight="medium">
                        Contact Us
                    </Text>
                </div>
                
            </div>
        </div>
    );
};

export default ContactUs;
