// @ts-nocheck
import React, { useState, useCallback } from "react";
import { Text, Card, Link, Checkbox, List } from "@shopify/polaris";
import styles from "../index.module.css";
const FAQ = () => {
  const [checked, setChecked] = useState(false);
  const handleChange = useCallback((newChecked) => setChecked(newChecked), []);
  return (
    <div>
      <div className={styles.ParentCheckboxAppearnce}>
        <div className={styles.SecondChildCheckboxStatus}>
          <Card>
            <Text as="h3" variant="bodyMd" fontWeight="medium">
              Frequently Asked Questions
            </Text>
            <p style={{ marginTop: "15px" }}></p>
            <Text as="p" variant="bodyMd" fontWeight="regular">
              Here are the most-asked questions that we are getting asked:
            </Text>
            <List type="bullet">
              <List.Item>
                <Link>
                  The terms and conditions checkbox is not appearing in my store
                </Link>
              </List.Item>
              <List.Item>
                <Link>
                  Where can I see the consent date and time in the order
                  details?
                </Link>
              </List.Item>
              <List.Item>
                <Link>
                  Where can I see the Checkout box In my theme editor?
                </Link>
              </List.Item>
              <List.Item>
                <Link>
                  Where can I see the consent date and time of when customers
                  have agreed to the Terms and Conditions?
                </Link>
              </List.Item>
              <List.Item>
                <Link>What's An Attribute Name?</Link>
              </List.Item>
            </List>
          </Card>
        </div>
        <div className={styles.FirstChildCheckboxStatus}>
          <Text as="h2" variant="bodyMd" fontWeight="medium">
            Frequently Asked Questions
          </Text>
        </div>
      </div>
    </div>
  );
};

export default FAQ;
