// @ts-nocheck
import React, { useState, useCallback } from "react";
import { Text, Card, Layout, Checkbox } from "@shopify/polaris";
import styles from "../index.module.css";
const AppearnceBuyItNow = () => {
    const [checked, setChecked] = useState(false);
    const handleChange = useCallback((newChecked) => setChecked(newChecked), []);
    return (
        <div>
            <div className={styles.ParentCheckboxAppearnce}>
                <div className={styles.FirstChildCheckboxStatus}>
                    <Text as="h2" variant="bodyMd" fontWeight="medium">
                        Show on Buy It Now Button Clicks
                    </Text>
                    <p style={{ marginTop: "10px" }}></p>
                    <Text as="p" tone="subdued">
                        You can make the checkbox appear inside a pop up modal when your
                        customers click on the Buy It Now button before they're being
                        redirected to the checkout page. Please note that orders that are
                        being made via the Buy It Now button won't have the consent date and
                        time log in the 'Additional Details' section due to payment
                        processors programmatic restrictions.{" "}
                    </Text>
                </div>
                <div className={styles.SecondChildCheckboxStatus}>
                    <Card>
                        <Layout>
                            <Layout.Section>
                                <Checkbox
                                    label="Open a pop up modal with the checkbox in it"
                                    checked={checked}
                                    onChange={handleChange}
                                />
                            </Layout.Section>
                            <Layout.Section>
                                <Checkbox
                                    label="Do nothing"
                                    checked={checked}
                                    onChange={handleChange}
                                />
                            </Layout.Section>
                        </Layout>
                    </Card>
                </div>
            </div>
        </div>
    );
};

export default AppearnceBuyItNow;
