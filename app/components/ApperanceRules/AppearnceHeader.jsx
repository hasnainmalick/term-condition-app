import React from "react";
import { Button,  Text } from "@shopify/polaris";
import { MobileBackArrowMajor } from "@shopify/polaris-icons";
import styles from "../index.module.css";
const AppearnceHeader = () => {
  return (
    <div className={styles.ApperacnceAccount}>
      <Button icon={MobileBackArrowMajor}></Button>{" "}
      <p className={styles.spacing}></p>
      <Text variant="headingLg" as="h5">
        Apperance Rules
      </Text>
    </div>
  );
};

export default AppearnceHeader;
