// @ts-nocheck
import React, { useState, useCallback } from 'react'
import { Text, Card, Layout, Checkbox } from '@shopify/polaris'
import styles from "../index.module.css"
const AppearnceSpecificCountries = () => {
    const [checked, setChecked] = useState(false);
    const handleChange = useCallback((newChecked) => setChecked(newChecked), []);
    return (
        <div>
            <div className={styles.ParentCheckboxAppearnce}>
                <div className={styles.FirstChildCheckboxStatus}>
                    <Text as="h2" variant="bodyMd" fontWeight="medium">
                    Show for Specific Countries
                    </Text>
                    <p style={{ marginTop: "10px" }}></p>
                    <Text as="p" tone="subdued">
                    The terms and conditions checkbox will only appear for visitors that are browsing from the specific countries you choose. The checkbox is determining the visitor's country using their IP geolocation.
                    </Text>
                </div>
                <div className={styles.SecondChildCheckboxStatus}>
                    <Card>
                        <Layout>
                            <Layout.Section>
                                <Checkbox
                                    label="
                                    Show the terms and conditions checkbox for visitors from all countries"
                                    checked={checked}
                                    onChange={handleChange}
                                />
                            </Layout.Section>
                            <Layout.Section>
                                <Checkbox
                                    label="Show the terms and conditions checkbox for visitors from specific countries only"
                                    checked={checked}
                                    onChange={handleChange}
                                />
                            </Layout.Section>
                        </Layout>
                    </Card>
                </div>
            </div>

        </div>
    )
}

export default AppearnceSpecificCountries
