import React from 'react'
import AppearnceHeader from './AppearnceHeader'
import { Divider, Page } from '@shopify/polaris'
import AppearnceSpecificProducts from './AppearnceSpecificProducts'
import AppearnceSpecificCountries from './AppearnceSpecificCountries';
import AppearnceBuyItNow from "./AppearnceBuyItNow"
const AppearnceRulesIndex = () => {
  return (
    <Page>
      <AppearnceHeader />
      <p style={{ marginBottom: "10px" }}></p>
      <Divider />
      <AppearnceSpecificProducts />
      <p style={{ marginTop: "9px" }}></p>
      <Divider />
      <AppearnceSpecificCountries/>
      <p style={{ marginTop: "9px" }}></p>
      <Divider />
      <AppearnceBuyItNow/>
      <p style={{ marginTop: "9px" }}></p>
      <Divider />
    </Page>
  )
}

export default AppearnceRulesIndex
