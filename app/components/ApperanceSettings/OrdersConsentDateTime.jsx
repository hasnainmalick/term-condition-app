// @ts-nocheck
import React, { useState, useCallback,useContext } from "react";
import {
  Button,
  Text,
  Divider,
  Card,
  Checkbox,
  Layout,
  Select,
  TextField,
  Toast,
} from "@shopify/polaris";
import { AppContext } from "~/context/AppContext";
import styles from "../index.module.css";
import { Form } from "@remix-run/react";
const OrdersConsentDateTime = () => {
  const [value, setValue] = useState("");
  const [errorMsg, setErrorMsg] = useState("");
  const [activeToast, setActiveToast] = useState(false);
  const {OrderConsent,handleAppearnceChecked,attributeName, handleAttribueName} = useContext(AppContext)
  const handleClick = () => {
    const formData = new FormData();
    formData.append("active", !OrderConsent);
    fetch("/app/appearnceSetting", {
      method: "POST",
      body: formData,
    });
    handleAppearnceChecked()
  };
  
  const toggleActive = useCallback(
    () => setActiveToast((active) => !active),
    []
  );
  const handleSave = () => {
    if (attributeName === "") {
      setErrorMsg("Please enter text");
      toggleActive();
    } else {
      const formData = new FormData();
      formData.append("attribute", attributeName);
      fetch("/app/appearnceSetting", {
        method: "POST",
        body: formData,
      });
      setErrorMsg("Successfully saved");
      toggleActive();
    }
  };
  const toastMarkup = activeToast ? (
    <Toast content={errorMsg} onDismiss={toggleActive} />
  ) : null;

  return (
    <div>
      <div className={styles.ParentCheckboxAppearnce}>
        <div className={styles.SecondChildCheckboxStatus}>
          <Layout>
            <Layout.Section>
              <Card>
                <div className={styles.Header}>
                  <div style={{ display: "flex" }}>
                    The consent date & time is{" "}
                    {OrderConsent ?(
                      <p
                        style={{
                          color: "darkgreen",
                          fontWeight: "bold",
                          margin: "0 4px",
                        }}
                      >
                        being logged
                      </p>
                    ) :  (
                      <p
                        style={{
                          color: "red",
                          fontWeight: "bold",
                          margin: "0 4px",
                        }}
                      >
                        {" "}
                        not being logged
                      </p>
                    )}{" "}
                  </div>
                  {OrderConsent ? ( <Button destructive onClick={handleClick}>
                      Deactivate
                    </Button>
                    
                  ) : (
                    <Button primary onClick={handleClick}>
                    Activate Consent Logging
                  </Button>
                  )}
                </div>
              </Card>
            </Layout.Section>
            <Layout.Section>
              <Card>
                <Form>
                  <TextField
                    label="Attribute Name"
                    value={attributeName}
                    onChange={handleAttribueName}
                    autoComplete="off"
                  />
                  <p style={{ marginTop: "10px" }}></p>
                  <Text as="p" tone="subdued">
                    <a traget="_blank" href="https://google.com">
                      Click here
                    </a>{" "}
                    to learn where you can see the consent date and time of when
                    your customers have agreed the terms and conditions.
                  </Text>
                  <div className={styles.OrderBTN}>
                    <Button primary submit onClick={handleSave}>
                      Save
                    </Button>
                    {toastMarkup}
                  </div>
                </Form>
              </Card>
            </Layout.Section>
          </Layout>
        </div>
        <div className={styles.FirstChildCheckboxStatus}>
          <Text as="h2" variant="bodyMd" fontWeight="medium">
          Checkbox Date and Time Log
          </Text>
          <p style={{ marginTop: "10px" }}></p>
          <Text as="p" tone="subdued">
          When activated this will record your system’s date and time of when the checkbox was ticked for that specific order. This will be displayed in the notes section of your order.
          </Text>
        </div>
      </div>
    </div>
  );
};

export default OrdersConsentDateTime;
