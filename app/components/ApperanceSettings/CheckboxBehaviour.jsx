// @ts-nocheck
import React, { useState, useCallback,useContext,useEffect } from "react";
import { MobileBackArrowMajor } from "@shopify/polaris-icons";
import {
  Button,
  Text,
  Divider,
  Card,
  Checkbox,
  Layout,
  Select,
  Toast,
} from "@shopify/polaris";
import styles from "../index.module.css";
import { AppContext } from "~/context/AppContext";
const CheckboxBehaviour = () => {

  const { handleTabChange,checked,
    optionalChecked,
    handleCheckboxChange} = useContext(AppContext)

  const [activeToast, setActiveToast] = useState(false);
  const toggleActive = useCallback(
    () => setActiveToast((active) => !active),
    []
  );
  const toastMarkup = activeToast ? (
    <Toast content="Successfully saved" onDismiss={toggleActive} />
  ) : null;
 
  
  const handleSelectChange = () => {
    handleCheckboxChange(!checked, false);
    toggleActive();
  };
  
  const handleOptionalSelectChange = () => {
    handleCheckboxChange(false, !optionalChecked);
    toggleActive();
  };
  
  
  const options = [
    { label: "Open links in a new tab", value: "today" },
    // { label: "Open link in a pop up window", value: "yesterday" },
    // { label: "Open link in the same window", value: "lastWeek" },
  ];
  return (
    <div>
      <div className={styles.ApperacnceAccount}>
        <Button
          icon={MobileBackArrowMajor}
          onClick={() => {
            handleTabChange(0);
          }}
        ></Button>{" "}
        <p className={styles.spacing}></p>
        <Text variant="headingLg" as="h5">
        Appearance Settings
        </Text>
      </div>
      <p style={{ marginBottom: "10px" }}></p>
      <Divider />
      <div className={styles.ParentCheckboxAppearnce}>
      <div className={styles.SecondChildCheckboxStatus}>
          <Card>
            <Text as="h3" variant="bodyMd" fontWeight="medium">
              Checkbox Default Appearance
            </Text>
            <p style={{ marginTop: "10px" }}></p>
            <Layout>
              <Layout.Section>
                <Checkbox
                 type="checkbox"
                 checked={checked}
                 onChange={handleSelectChange}
                  label="Make the checkbox ticked by default"
                />
              </Layout.Section>
              <Layout.Section>
                <Checkbox
                  label="Make the checkbox optional"
                  type="checkbox"
                  checked={optionalChecked}
                  onChange={handleOptionalSelectChange}
                />
              </Layout.Section>
              {toastMarkup}
            </Layout>
            {/* <p style={{ marginTop: "20px" }}></p> */}
            {/* <Divider /> */}
            {/* <p style={{ marginBottom: "20px" }}></p>
            <Select
              label="Links Opening Behaviour"
              options={options}
              onChange={handleSelectChange}
              value={selected}
            /> */}
          </Card>
        </div>
        <div className={styles.FirstChildCheckboxStatus}>
          <Text as="h2" variant="bodyMd" fontWeight="medium">
          Checkbox Options
          </Text>
          <p style={{ marginTop: "10px" }}></p>
          <Text as="p" tone="subdued">
          Checkbox ticked by default : will have the box automatically ticked in the checkout
          <br/>
          Checkbox optional - Will allow customers to proceed to the next step of checkout without ticking the checkbox
          </Text>
        </div>
        
      </div>
    </div>
  );
};
export default CheckboxBehaviour;




