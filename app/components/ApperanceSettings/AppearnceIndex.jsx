import React from "react";
import CheckboxBehaviour from "./CheckboxBehaviour";
import { Page, Divider, Frame } from "@shopify/polaris";
import OrdersConsentDateTime from "./OrdersConsentDateTime";
const AppearnceIndex = () => {
  return (
    <Page >
      <Frame>
      <CheckboxBehaviour />
      <p style={{ marginBottom: "20px" }}></p>
      <Divider />
      <OrdersConsentDateTime/>
      </Frame>
    </Page>
  );
};

export default AppearnceIndex;
