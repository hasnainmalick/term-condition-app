// @ts-nocheck
import { Box, Button, Divider, Text } from "@shopify/polaris";
import React,{useState, useEffect} from "react";
import styles from "../index.module.css"
import {
    CustomersMajor
  } from '@shopify/polaris-icons';
const AccountSetting = () => {
 

  return (
    <Box  borderColor="border">
      
      <div className={styles.Header}>
        <Text variant="headingLg" as="h5">
          Welcome to T&C: Terms and Conditions Checkbox{" "}
        </Text>
        <Button
          // @ts-ignore
          url="/app/billing"
          // onClick={handleClick}
          variant="tertiary"
          icon={CustomersMajor}
        >
          Account Settings
        </Button>
      </div>
      <p style={{marginBottom:"10px"}}></p>
      <Divider/>
    </Box>
  );
};

export default AccountSetting;
