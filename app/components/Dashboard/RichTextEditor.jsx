// @ts-nocheck
import {
  Box,
  Button,
  Card,
  Divider,
  TextField,
  Text,
  Layout,
  Toast
} from "@shopify/polaris";
import React, { useState, useCallback, useContext } from "react";
import styles from "../index.module.css";
import { Form } from "@remix-run/react";
import { AppContext } from "~/context/AppContext";
export default function RichTextEditor() {
  const [active, setActive] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const { input, handleChange, altValue, handleAltChange,handleEditorLink,editorLink ,editorHyperLinkText, handleEditorHyperText} =
    useContext(AppContext);
  const toggleActive = useCallback(() => setActive((active) => !active), []);
  const handleClick = () => {
    if (input === "") {
      setErrorMsg("Please enter text");
      toggleActive();
    } else {
      setErrorMsg("Successfully saved");
      toggleActive();
    }
  };
  const toastMarkup = active ? (
    <Toast content={errorMsg} onDismiss={toggleActive} />
  ) : null;

  return (
    <Box>
      <div style={{ marginTop: "10px" }}></div>
      <Divider />
      <div className={styles.ParentCheckboxStatus}>
        <div className={styles.SecondChildCheckboxStatus}>
          {/* <Frame> */}
          <Card>
            <Form method="POST">
              <Layout>
                <Layout.Section>
                  <TextField
                    label="Terms and Conditions Text"
                    value={input}
                    name="ErrorMsg"
                    onChange={handleChange}
                    multiline={4}
                    autoComplete="off"
                  />
                </Layout.Section>
                
                <Layout.Section>
                  <TextField
                    label="Terms and Conditions Hyperlink Text"
                    value={editorHyperLinkText}
                    onChange={handleEditorHyperText}
                    name="editorHyperText"
                    placeholder="Click here"
                    autoComplete="off"
                  />
                </Layout.Section>
                <Layout.Section>
                  <TextField
                    label="Terms and Conditions Hyperlink"
                    value={editorLink}
                    onChange={handleEditorLink}
                    name="editorLinks"
                    placeholder="https://privacypolicy.com"
                    helpText="If you do not provide the hyperlink here, it will not display on the checkout page.                    "
                    autoComplete="off"
                  />
                </Layout.Section>
                <Layout.Section>
                  <TextField
                    label="Alert Text"
                    value={altValue}
                    onChange={handleAltChange}
                    name="altMsg"
                    multiline={4}
                    helpText="This text will be displayed should a customer try to got to the next step in the checkout without ticking the checkbox. "
                    autoComplete="off"
                  />
                </Layout.Section>
              </Layout>
              <div className={styles.OrderBTN}>
                <Button submit primary onClick={handleClick}>
                  Save
                </Button>
                {toastMarkup}
              </div>
            </Form>
          </Card>
          {/* </Frame> */}
        </div>
        <div className={styles.FirstChildCheckboxStatus}>
          <Text as="h2" variant="bodyMd" fontWeight="medium">
            Basic Settings
          </Text>
          <Text as="p" tone="subdued">
          Due to certain limitations with shopify checkout extensions you will have to add the text and hyperlink text separately. The terms and conditions hyperlink text will appear after the terms and conditons text on the front end.
          </Text>
        </div>
      </div>
    </Box>
  );
}
