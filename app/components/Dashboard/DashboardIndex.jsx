// @ts-nocheck
import React from 'react'
import AccountSetting from './AccountSetting'
import { Page , Divider, Frame } from '@shopify/polaris'
import CheckboxPreview from "./CheckboxPreview"
import CheckboxStatus from "./CheckboxStatus"
import RichTextEditor from "./RichTextEditor"
const DashboardIndex = () => {
  return (
    <Page>
      <Frame>
      <AccountSetting/>
      <CheckboxStatus/>
      <RichTextEditor />
      <p style={{marginBottom:"10px"}}></p>
      <Divider/>
      <CheckboxPreview/>
      </Frame>
    </Page>
  )
}

export default DashboardIndex