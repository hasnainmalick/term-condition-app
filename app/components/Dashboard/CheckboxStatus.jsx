// @ts-nocheck
import { Box, Button, Card, Divider, Text, Toast } from "@shopify/polaris";
import React, { useState, useContext, useCallback } from "react";
import styles from "../index.module.css";
import { AppContext } from "~/context/AppContext";
export default function CheckboxStatus() {
  const [activeToast, setActiveToast] = useState(false);
  const { active, handleActiveCheckbox } = useContext(AppContext);
  const toggleActive = useCallback(() => {
    setActiveToast((active) => !active);
  }, []);
  const toastMarkup = activeToast ? (
    <Toast content="Successfully saved" onDismiss={toggleActive} />
  ) : (
    ""
  );
  const handleChange = async () => {
    toggleActive();
    const formData = new FormData();
    formData.append("active", !active);
    await fetch("/app/checkboxStatus", {
      method: "POST",
      body: formData,
    });
    handleActiveCheckbox(active);
  };
  return (
    <Box>
      <Divider />
      <div className={styles.ParentCheckboxStatus}>
        <div className={styles.SecondChildCheckboxStatus}>
          <Card>
            <div className={styles.Header}>
              <div style={{ display: "flex" }}>
                The Terms and Conditions Checkbox is{" "}
                {active ? (
                  <p style={{ color: "darkgreen", margin: "0 4px" }}> active</p>
                ) : (
                  <p style={{ color: "red", margin: "0 4px" }}>non active</p>
                )}{" "}
                in your store.
              </div>{" "}
              {active ? (
                <Button destructive onClick={handleChange}>
                  Disable
                </Button>
              ) : (
                <Button primary onClick={handleChange}>
                  Enable
                </Button>
              )}
              {toastMarkup}
            </div>
          </Card>
        </div>
        <div className={styles.FirstChildCheckboxStatus}>
          <Text as="h2" variant="bodyMd" fontWeight="medium">
            Checkbox Status
          </Text>
          <Text as="p" tone="subdued">
            Enable or disable the Terms and Conditions Checkbox in your store
          </Text>
        </div>
      </div>
    </Box>
  );
}
