// @ts-nocheck
import React,{useCallback, useState, useContext} from "react";
import { Card,Checkbox, Button,Text } from "@shopify/polaris";
import styles from "../index.module.css"
import { AppContext } from "~/context/AppContext";
const CheckboxPreview = () => {
    const [checked, setChecked] = useState(false);
    const { input ,editorHyperLinkText} = useContext(AppContext)
  const handleChange = useCallback(
    (newChecked) => setChecked(newChecked),
    [],
  );
  return (
    <>
    <div style={{marginTop:"10px"}}></div>
    <Card>
      <Text as="h2" variant="bodyMd" fontWeight="medium">
        Checkbox Preview
      </Text>
      <div className={styles.LivePreviewCheckbox}>
      <Checkbox
      // eslint-disable-next-line jsx-a11y/anchor-is-valid
      label={input !== "" ? (
        <span>
          {input} <a href="https://google.com" target="_blank" rel="noreferrer">{editorHyperLinkText}</a>
        </span>
      ) : "I agree to the terms and conditions"} checked={checked}
      onChange={handleChange}
    />
    <Button>Checkout</Button>
    </div>
    </Card>
    </>
  );
};

export default CheckboxPreview;
