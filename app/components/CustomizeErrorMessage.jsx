import React, { useState, useCallback } from "react";
import { Form, useActionData } from "@remix-run/react";
import { Toast, Card, Frame, Page, TextField, Button } from "@shopify/polaris";

const CustomizeErrorMessage = () => {
    const [value, setValue] = useState("");
    const [active, setActive] = useState(false);
    const data = useActionData();
    const toggleActive = useCallback(() => setActive((active) => !active), []);
    const toastMarkup = active ? (
        <Toast content="Successfully Saved" onDismiss={toggleActive} />
    ) : null;
    const handleChange = useCallback((newValue) => setValue(newValue), []);
    return (
        <div style={{ height: "200px" }}>
            <Frame>
                <Card>
                    <Form method="POST">
                        <TextField
                            label="Text Editor"
                            name="text"
                            value={value}
                            onChange={handleChange}
                            multiline={4}
                            autoComplete="off"
                            placeholder="Enter Error Message"
                        />
                        <div style={{ marginTop: "10px" }} />
                        <Button submit onClick={toggleActive} primary>
                            Save
                        </Button>
                        {toastMarkup}
                    </Form>
                </Card>
            </Frame>
        </div>
    );
};

export default CustomizeErrorMessage;
