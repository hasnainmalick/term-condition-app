// @ts-nocheck
// @ts-ignore
import { useState, useCallback, useContext, useEffect } from "react";
import { LegacyCard, Text, Tabs } from "@shopify/polaris";
import styles from "./index.module.css";
import { tabs } from "./tabs";
import DashboardIndex from "./Dashboard/DashboardIndex";
import AppearnceIndex from "./ApperanceSettings/AppearnceIndex";
// @ts-ignore
import AppearnceRulesIndex from "./ApperanceRules/AppearnceRulesIndex";
import HelpIndex from "./Help/HelpIndex";
import { AppContext } from "~/context/AppContext";
export default function TopbarTabs({
  ErrorandAltMsg,
  activeCheckbox,
  appearnceSetting,
}) {
  const {
    handleChange,
    handleAltChange,
    selected,
    handleTabChange,
    handleActiveCheckboxDB,
    handleEditorLink,
    handleAppearnceCheckedDB,
    handleAttribueName,
    handleOptionalCheck,
    handleDefaultCheck,
    handleEditorHyperText,
  } = useContext(AppContext);

  useEffect(() => {
    if (appearnceSetting.length > 0) {
      if (appearnceSetting[0]?.attribute_name) {
        handleAttribueName(appearnceSetting[0]?.attribute_name);
      }
      if (appearnceSetting[0]?.consent) {
        handleAppearnceCheckedDB(JSON.parse(appearnceSetting[0]?.consent));
      }
      if (appearnceSetting[0]?.checkbox_optional) {
        handleOptionalCheck(JSON.parse(appearnceSetting[0]?.checkbox_optional));
      }
      if (appearnceSetting[0]?.checkbox_by_default) {
        handleDefaultCheck(
          JSON.parse(appearnceSetting[0]?.checkbox_by_default)
        );
      }
    }
    if (activeCheckbox) {
      handleActiveCheckboxDB(JSON.parse(activeCheckbox));
    }
    if (ErrorandAltMsg.length > 0) {
      handleEditorLink(ErrorandAltMsg[0]?.editorLinks);
      handleChange(ErrorandAltMsg[0]?.errormsg);
      handleAltChange(ErrorandAltMsg[0]?.altmsg);
      handleEditorHyperText(ErrorandAltMsg[0]?.editorHyperLinkText);
    }
  }, []);

  return (
    <div>
      <div className={styles.Header} style={{ width: "100vw" }}>
        <Text variant="headingXl" as="h4">
          {tabs[selected].content}
        </Text>
        <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}></Tabs>
      </div>

      <LegacyCard.Section>
        {tabs[selected].id === "dashboard" && <DashboardIndex />}
        {tabs[selected].id === "apperance_setting" && <AppearnceIndex />}
        {/* {tabs[selected].id === "apperance_rules" && <AppearnceRulesIndex/>} */}
        {tabs[selected].id === "help" && <HelpIndex />}
      </LegacyCard.Section>
    </div>
  );
}
