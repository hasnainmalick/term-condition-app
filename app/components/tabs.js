export const tabs = [
    {
      id: 'dashboard',
      content: 'Dashboard',
      accessibilityLabel: 'dashboard',
      panelID: 'dashboard',
    },
    {
      id: 'apperance_setting',
      content: 'Appearance Settings',
      panelID: 'errormsg',
    },
    // {
    //   id: 'apperance_rules',
    //   content: 'Apperance Rules',
    //   panelID: 'apperance_rules',
    // },
    {
      id: 'help',
      content: 'Help',
      panelID: 'help',
    },
  ];